/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "buzzer_music.h"
#include "lcd.h"
#include "lsm6dsox_fifo.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t key_status[5] = {0x00};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint16_t dutyCycle = 0;
void hu_xi_deng()
{
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, dutyCycle);
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, dutyCycle);
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, dutyCycle);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, dutyCycle);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
	  HAL_Delay(2);
}

//十进制转BCD码
uint32_t decimal_bcd_code(uint32_t decimal)
{
	uint32_t i,sum = 0; //i计数变量，sum返回的BCD码
	for (i = 0; decimal > 0; i++)
	{
		sum |= ((decimal % 10) << (4 * i));
		decimal /= 10;
	}
	return sum;
}

//bcd转十进制
uint32_t bcd_decimal_code(uint32_t bcd)
{
	uint32_t i,sum = 0, c = 1; //sum返回十进制，i循环计数，c每次翻10倍
	for (i = 1; bcd > 0; i++)
	{
		if (i >= 2)
		{
			c *= 10;
		}
		sum += (bcd % 16) * c;
		bcd /= 16; //  /16同理与十进制除10将小数点左移一次，%16也同理
	}
	return sum;
}
#define BEGIN_YEAR 2020
uint8_t get_week_index(uint8_t weekday)
{
	switch(weekday)
	{
		case RTC_WEEKDAY_MONDAY:
		{
			return 7;
			
		}
		case RTC_WEEKDAY_TUESDAY:
		{
			return 8;
			
		}
		case RTC_WEEKDAY_WEDNESDAY:
		{
			return 9;
			
		}
		case RTC_WEEKDAY_THURSDAY:
		{
			return 10;
			
		}
		case RTC_WEEKDAY_FRIDAY:
		{
			return 11;
			
		}
		case RTC_WEEKDAY_SATURDAY:
		{
			return 12;
			
		}
		case RTC_WEEKDAY_SUNDAY:
		{
			return 2;
			
		}
	}
	return 0;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint32_t ADC_Value;
	uint32_t ADC_Value2;
	RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_RTC_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	printf("before lcd init\r\n");
	LCD_Init();
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);
	
	printf("before lsm6dsox init\r\n");
	lsm6dsox_fifo_init();
	printf("after lsm6dsox init\r\n");
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		hu_xi_deng();
		if(key_status[0])
		{
			LCD_ShowString(20,60,16,"key1 pressed !");
			key_status[0]=0x00;
			play_music();
		}
		if(key_status[1])
		{
			key_status[1]=0x00;
			LCD_ShowString(20,60,16,"key2 pressed !");
		}
		if(key_status[2])
		{
			key_status[2]=0x00;
			LCD_ShowString(20,60,16,"key3 pressed !");
		}
		if(key_status[3])
		{
			key_status[3]=0x00;
			LCD_ShowString(20,60,16,"key4 pressed !");
		}
		if(key_status[4])
		{
			key_status[4]=0x00;
			LCD_ShowString(20,60,16,"keyb pressed !");
		}
		/*
		LCD_Fill(0,0,10,10,RED);
		LCD_Fill(230,0,240,10,BLUE);
		LCD_Fill(0,310,10,320,YELLOW);
		LCD_Fill(230,310,240,320,BROWN);
		*/
		
		LCD_Fill(0,0,10,10,RED);
		LCD_Fill(310,0,320,10,BLUE);
		LCD_Fill(0,230,10,240,YELLOW);
		LCD_Fill(310,230,320,240,BROWN);
		
		for(uint16_t i=0;i<13;i++)
		{
			LCD_ShowChinese(10+16*i,0,i);
		}
		HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
		HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BCD);
		
		sDate.Year = bcd_decimal_code(sDate.Year);
		sDate.Month = bcd_decimal_code(sDate.Month);
		sDate.Date = bcd_decimal_code(sDate.Date);
		
		sTime.Hours = bcd_decimal_code(sTime.Hours);
		sTime.Minutes = bcd_decimal_code(sTime.Minutes);
		sTime.Seconds = bcd_decimal_code(sTime.Seconds);
		
		LCD_ShowNum(20,20,2020+sDate.Year,4,16);// 2021
		LCD_ShowChinese(20+16*2,20,0);// 年
		LCD_ShowNum(20+16*3,20,sDate.Month,2,16);// 12
		LCD_ShowChinese(20+16*4,20,1);// 月
		LCD_ShowNum(20+16*5,20,sDate.Date,2,16);// 12
		LCD_ShowChinese(20+16*6,20,1);// 日
		LCD_ShowChinese(20+16*8,20,6);// 周
		LCD_ShowChinese(20+16*9,20,get_week_index(sDate.WeekDay));// 一
		
		LCD_ShowNum(20,40,sTime.Hours,2,16);// 19
		LCD_ShowChinese(20+16*1,40,3);// 时
		LCD_ShowNum(20+16*2,40,sTime.Minutes,2,16);// 12
		LCD_ShowChinese(20+16*3,40,4);// 分
		LCD_ShowNum(20+16*4,40,sTime.Seconds,2,16);// 12
		LCD_ShowChinese(20+16*5,40,5);// 秒
		
		LCD_ShowString(20,80,16,"ADC value = ");
		HAL_ADC_Start(&hadc1);     //启动ADC转换
		HAL_ADC_PollForConversion(&hadc1, 50);   //等待转换完成，50为最大等待时间，单位为ms
		if(HAL_IS_BIT_SET(HAL_ADC_GetState(&hadc1), HAL_ADC_STATE_REG_EOC))
		{
				ADC_Value = HAL_ADC_GetValue(&hadc1);   //获取通道8的AD值
			
			LCD_ShowNum(20+12*8,80,ADC_Value,4,16);
		}
		
		dutyCycle = ADC_Value*1000/4096;
		LCD_ShowString(20,100,16,"dutyCycle = ");
		LCD_ShowNum(20+8*12,100,dutyCycle,4,16);
		LCD_ShowString(20,120,16,"ADC value = ");
		
		HAL_ADC_Start(&hadc1);     //启动ADC转换
		HAL_ADC_PollForConversion(&hadc1, 50);   //等待转换完成，50为最大等待时间，单位为ms
		if(HAL_IS_BIT_SET(HAL_ADC_GetState(&hadc1), HAL_ADC_STATE_REG_EOC))
		{
				ADC_Value2 = HAL_ADC_GetValue(&hadc1);   //获取通道4的AD值
			
			LCD_ShowNum(20+12*8,120,ADC_Value2,4,16);
		}
		
		lsm6dsox_fifo_run();
		sprintf((char *)tx_buffer,"Acceleration [mg]:(x, y, z)");
		LCD_ShowString(20,140,16,tx_buffer);
		sprintf((char *)tx_buffer,"(%4.2f, %4.2f, %4.2f)",acceleration_mg[0], acceleration_mg[1], acceleration_mg[2]);
		LCD_Fill(0,160,LCD_WIDTH-1,176,BACK_COLOR);
		LCD_ShowString(0,160,16,tx_buffer);
		sprintf((char *)tx_buffer,"Angular rate [mdps]:(x, y, z)");
		LCD_ShowString(20,180,16,tx_buffer);
		sprintf((char *)tx_buffer,"(%4.2f, %4.2f, %4.2f)",angular_rate_mdps[0], angular_rate_mdps[1], angular_rate_mdps[2]);
		LCD_Fill(0,200,LCD_WIDTH-1,216,BACK_COLOR);
		LCD_ShowString(0,200,16,tx_buffer);
		
		HAL_Delay(100);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
