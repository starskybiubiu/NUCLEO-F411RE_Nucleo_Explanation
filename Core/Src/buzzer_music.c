#include "buzzer_music.h"
#include "tim.h"

/******************************����������*************************************/
#define ARRAY_LEN(a)					(sizeof(a)/sizeof(a[0]))
#define BASE_FRQ				1000000

const uint32_t tone[][2][3] = {
	{ {261, 523, 1046}, {277, 554, 1109} },
	{ {294, 587, 1175}, {311, 622, 1245} },
	{ {330, 659, 1318}, {0, 0, 0} },
	{ {349, 698, 1397}, {370, 740, 1480} },
	{ {392, 784, 1568}, {415, 831, 1661} },
	{ {440, 880, 1760}, {466, 932, 1865} },
	{ {494, 988, 1976}, {0, 0, 0} }
};

uint8_t BeiJing1[][3] =
{
		{3, 0, 1}, {5, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {6, 0, 0}, {1, 0, 1}, {3, 0, 1}, {2, 0, 1}, {0, 0, 0}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {6, 0, 0}, {2, 0, 1}, {1, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {5, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {6, 0, 0}, {1, 0, 1}, {3, 0, 1}, {2, 0, 1}, {0, 0, 0}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {6, 0, 0}, {2, 0, 1}, {1, 0, 1}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {5, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {1, 0, 1}, {5, 0, 1}, {6, 0, 1}, {2, 0, 1}, {0, 0, 0},
		{5, 0, 0}, {3, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {0, 0, 0},

		{3, 0, 1}, {5, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {6, 0, 0}, {1, 0, 1}, {3, 0, 1}, {2, 0, 1}, {0, 0, 0}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {6, 0, 0}, {2, 0, 1}, {1, 0, 1}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {5, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {1, 0, 1}, {5, 0, 1}, {6, 0, 1}, {2, 0, 1}, {0, 0, 0},
		{6, 0, 0}, {3, 0, 1}, {2, 0, 1}, {2, 0, 1}, {1, 0, 1}, {1, 0, 1},

		{0, 0, 0}, {0, 0, 0}, {3, 0, 1}, {5, 0, 1}, {0, 0, 0},
		{1, 0, 2}, {5, 0, 1}, {6, 0, 1}, {0, 0, 0}, {6, 0, 1}, {5, 0, 1}, {3, 0, 1}, {3, 0, 1}, {5, 0, 1}, {5, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {5, 0, 1}, {6, 0, 1}, {1, 0, 2}, {2, 0, 2}, {1, 0, 2}, {5, 0, 1}, {3, 0, 1}, {2, 0, 1}, {5, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {5, 0, 1}, {1, 0, 2}, {5, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{1, 0, 2}, {2, 0, 2}, {1, 0, 2}, {5, 0, 1}, {3, 0, 1}, {5, 0, 1}, {7, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {3, 0, 2}, {2, 0, 2}, {1, 0, 2}, {1, 0, 2}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}

};

uint8_t BeiJing2[][3] =
{
		{3, 0, 1}, {5, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {6, 0, 0}, {1, 0, 1}, {3, 0, 1}, {2, 0, 1}, {0, 0, 0}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {6, 0, 0}, {2, 0, 1}, {1, 0, 1}, {0, 0, 0},
		{2, 0, 1}, {1, 0, 1}, {6, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {2, 0, 1},
		{3, 0, 1}, {6, 0, 1}, {5, 0, 1}, {5, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{2, 0, 1}, {3, 0, 1}, {2, 0, 1}, {1, 0, 1}, {5, 0, 1}, {6, 0, 1}, {2, 0, 1}, {0, 0, 0},
		{6, 0, 0}, {3, 0, 1}, {2, 0, 1}, {2, 0, 1}, {1, 0, 1}, {1, 0, 1}, {0, 0, 0},

		{3, 0, 1}, {5, 0, 1}, {1, 0, 2}, {5, 0, 1}, {6, 0, 1}, {0, 0, 0}, {6, 0, 1}, {5, 0, 1}, {3, 0, 1}, {3, 0, 1}, {5, 0, 1}, {5, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {5, 0, 1}, {6, 0, 1}, {1, 0, 2}, {2, 0, 2}, {1, 0, 2}, {5, 0, 1}, {3, 0, 1}, {2, 0, 1}, {5, 0, 1}, {3, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {5, 0, 1}, {1, 0, 2}, {5, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{1, 0, 2}, {2, 0, 2}, {1, 0, 2}, {5, 0, 1}, {3, 0, 1}, {5, 0, 1}, {7, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {3, 0, 2}, {2, 0, 2}, {1, 0, 2}, {1, 0, 2}, {0, 0, 0},

		{3, 0, 1}, {5, 0, 1}, {1, 0, 2}, {5, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{1, 0, 2}, {2, 0, 2}, {1, 0, 2}, {5, 0, 1}, {3, 0, 1}, {5, 0, 1}, {7, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {3, 0, 2}, {2, 0, 2}, {1, 0, 2}, {1, 0, 2}, {0, 0, 0}, {0, 0, 0},

		{3, 0, 1}, {5, 0, 1}, {1, 0, 2}, {5, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{1, 0, 2}, {2, 0, 2}, {1, 0, 2}, {5, 0, 1}, {3, 0, 1}, {5, 0, 1}, {7, 0, 1}, {6, 0, 1}, {0, 0, 0},
		{3, 0, 1}, {2, 0, 1}, {3, 0, 1}, {5, 0, 1}, {3, 0, 2}, {2, 0, 2}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {1, 0, 2},
};

uint8_t molihua[][3] =
{
	{3, 0, 1},{0, 0, 0},{3, 0, 1}, {5, 0, 1}, {6, 0, 1}, {1, 0, 2},{1, 0, 2},{6, 0, 1},{5, 0, 1},{0,0,0}, {5, 0, 1}, {6, 0, 1},{5, 0, 1},{0, 0, 0},
};

uint8_t tiankongzhicheng[][3]=
{
	{6,0,1},{7,0,1},{1,0,2},{0,0,0},{7,0,1},{1,0,2},{0,0,0},{3,0,2},{7,0,1},{0,0,0},
	{3,0,1},{3,0,1},{6,0,1},{0,0,0},{5,0,1},{6,0,1},{0,0,0},{1,0,2},{5,0,1},{0,0,0},
	{3,0,1},{3,0,1},{4,0,1},{0,0,0},{3,0,1},{4,0,1},{1,0,2},{0,0,0},{3,0,1},{0,0,0},{3,0,1},{1,0,2},{1,0,2},{0,0,0},
	{7,0,1},{4,1,1},{4,1,1},{7,0,1},{7,0,1},{0,0,0},
};

void beep_set(uint32_t frq){

	  TIM_MasterConfigTypeDef sMasterConfig = {0};
	  TIM_OC_InitTypeDef sConfigOC = {0};

	  htim1.Init.Period = BASE_FRQ / frq - 1;
	  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sConfigOC.OCMode = TIM_OCMODE_PWM1;
	  sConfigOC.Pulse = htim1.Init.Period / 2;
	  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  HAL_TIM_MspPostInit(&htim1);
}

void beep_cry(uint32_t delay)
{
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_Delay(delay);
	HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
}

void play_music(void)
{
	uint16_t i;
	/*
	for(i=0; i<ARRAY_LEN(BeiJing1); i++){
		  if(BeiJing1[i][0] != 0){
			  beep_set( tone[BeiJing1[i][0]-1][BeiJing1[i][1]][BeiJing1[i][2]] );
		  	  beep_cry(365);
		  }
		  else{
			  HAL_Delay(250);
		  }
	  }
	*/
	/*
	  for(i=0; i<ARRAY_LEN(BeiJing2); i++){
	  	 if(BeiJing2[i][0] != 0){
	  	     beep_set( tone[BeiJing2[i][0]-1][BeiJing2[i][1]][BeiJing2[i][2]] );
	         beep_cry(365);
	  	 }
	  	 else{
	  	     HAL_Delay(250);
	  	 }
	  }
		*/
		/*
		for(i=0; i<ARRAY_LEN(molihua); i++){
		  if(molihua[i][0] != 0){
			  beep_set( tone[molihua[i][0]-1][molihua[i][1]][molihua[i][2]] );
		  	  beep_cry(365);
		  }
		  else{
			  HAL_Delay(250);
		  }
	  }
		*/
		for(i=0; i<ARRAY_LEN(tiankongzhicheng); i++){
		  if(tiankongzhicheng[i][0] != 0){
			  beep_set( tone[tiankongzhicheng[i][0]-1][tiankongzhicheng[i][1]][tiankongzhicheng[i][2]] );
		  	  beep_cry(365);
		  }
		  else{
			  HAL_Delay(250);
		  }
	  }
	  //beep_set(1046);
	  //HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	  //HAL_Delay(3000);
	  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
}

/******************************����������*************************************/
