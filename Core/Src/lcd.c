#include "lcd.h"
#include "spi.h"
#include "font.h"
#include "chinese_font.h"

// 尺寸定义，默认是竖屏模式，当选择横屏模式，需要调换两个参数。
uint16_t LCD_WIDTH = LCD_X;
uint16_t LCD_HEIGHT = LCD_Y;

// LCD的画笔颜色和背景色
uint16_t POINT_COLOR = 0x0000; //画笔颜色
uint16_t BACK_COLOR = 0xFFFF;  //背景色

//写寄存器函数
//reg:寄存器值
void LCD_WR_REG(uint8_t reg)
{
	HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, &reg, 1, 0xFFFF);
}
//写LCD数据
//data:要写入的值
void LCD_WR_DATA(uint8_t data)
{
	HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_SET);
	HAL_SPI_Transmit(&hspi1, &data, 1, 0xFFFF);
}

// ILI9341控制器屏幕初始化
void LCD_ILI9341_Init(void)
{
	//************* Start Initial Sequence **********//

	LCD_WR_REG(0xCF);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0xc3);
	LCD_WR_DATA(0X30);

	LCD_WR_REG(0xED);
	LCD_WR_DATA(0x64);
	LCD_WR_DATA(0x03);
	LCD_WR_DATA(0X12);
	LCD_WR_DATA(0X81);

	LCD_WR_REG(0xE8);
	LCD_WR_DATA(0x85);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x78); //7a

	LCD_WR_REG(0xCB);
	LCD_WR_DATA(0x39);
	LCD_WR_DATA(0x2C);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x34);
	LCD_WR_DATA(0x02);

	LCD_WR_REG(0xF7);
	LCD_WR_DATA(0x20);

	LCD_WR_REG(0xEA);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0xC0);  //Power control
	LCD_WR_DATA(0x1b); //VRH[5:0]  21

	LCD_WR_REG(0xC1);  //Power control
	LCD_WR_DATA(0x12); //SAP[2:0];BT[3:0]

	LCD_WR_REG(0xC5);
	LCD_WR_DATA(0x32); //32
	LCD_WR_DATA(0x3C);

	LCD_WR_REG(0xC7);  //VCM control2
	LCD_WR_DATA(0xAF); //9e

	LCD_WR_REG(0x36);  // Memory Access Control
	LCD_WR_DATA(0x08); // FPC D:0X08   68

	LCD_WR_REG(0x21);

	LCD_WR_REG(0x3A);
	LCD_WR_DATA(0x55);

	LCD_WR_REG(0xB1);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x1b);
	LCD_WR_REG(0xB4);
	LCD_WR_DATA(0x04);

	LCD_WR_REG(0xB6); // Display Function Control
	LCD_WR_DATA(0x0A);
	LCD_WR_DATA(0xA2);

	LCD_WR_REG(0xF6);
	LCD_WR_DATA(0x01);
	LCD_WR_DATA(0x30);

	LCD_WR_REG(0xF2); // 3Gamma Function Disable
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0x26); //Gamma curve selected
	LCD_WR_DATA(0x01);

	LCD_WR_REG(0xE0); //Set Gamma
	LCD_WR_DATA(0x0F);
	LCD_WR_DATA(0x1d);
	LCD_WR_DATA(0x1a);
	LCD_WR_DATA(0x0a);
	LCD_WR_DATA(0x0d);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x49);
	LCD_WR_DATA(0X66);
	LCD_WR_DATA(0x3b);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x11);
	LCD_WR_DATA(0x01);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x05);
	LCD_WR_DATA(0x04); //04

	LCD_WR_REG(0XE1); //Set Gamma '
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x1d);
	LCD_WR_DATA(0x02);
	LCD_WR_DATA(0x0f);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x36);
	LCD_WR_DATA(0x13);
	LCD_WR_DATA(0x4c);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x13);
	LCD_WR_DATA(0x0f);
	LCD_WR_DATA(0x2E);
	LCD_WR_DATA(0x2f);
	LCD_WR_DATA(0x05); //0F

	LCD_WR_REG(0x11); //Exit Sleep
	HAL_Delay(120);	  //ms
	LCD_WR_REG(0x29); //Display on
}

// ST7789V控制器屏幕初始化
void LCD_ST7789V_Init(void)
{
	//************* Start Initial Sequence **********//
	LCD_WR_REG(0x11);

	HAL_Delay(120); //ms

	LCD_WR_REG(0x36);
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0x3a);
	LCD_WR_DATA(0x55);
	//--------------------------------ST7789V Frame rate setting----------------------------------//
	LCD_WR_REG(0xb0);
	LCD_WR_DATA(0x11);
	LCD_WR_DATA(0xf0);
	LCD_WR_REG(0xb1);
	LCD_WR_DATA(0x40);
	LCD_WR_DATA(0x10);
	LCD_WR_DATA(0x12);

	LCD_WR_REG(0xb2);
	LCD_WR_DATA(0x0c);
	LCD_WR_DATA(0x0c);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x33);
	LCD_WR_DATA(0x33);

	LCD_WR_REG(0xb7);
	LCD_WR_DATA(0x75); //VGH=14.97  VGL=-10.43 //35

	LCD_WR_REG(0xbb);
	LCD_WR_DATA(0x19);

	LCD_WR_REG(0xc0);
	LCD_WR_DATA(0x2c);

	LCD_WR_REG(0xc2);
	LCD_WR_DATA(0x01);

	LCD_WR_REG(0xc3);
	LCD_WR_DATA(0x0b); //10

	LCD_WR_REG(0xc4);
	LCD_WR_DATA(0x20);

	LCD_WR_REG(0xc6);
	LCD_WR_DATA(0x0f);

	LCD_WR_REG(0xd0);
	LCD_WR_DATA(0xa4);
	LCD_WR_DATA(0xa1);

	LCD_WR_REG(0xb0);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0xd0);

	LCD_WR_REG(0xE0);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x0C);
	LCD_WR_DATA(0x0E);
	LCD_WR_DATA(0x0E);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x32);
	LCD_WR_DATA(0x44);
	LCD_WR_DATA(0x44);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x10);
	LCD_WR_DATA(0x0E);
	LCD_WR_DATA(0x12);
	LCD_WR_DATA(0x16);

	LCD_WR_REG(0xE1);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x03);
	LCD_WR_DATA(0x0A);
	LCD_WR_DATA(0x0D);
	LCD_WR_DATA(0x0D);
	LCD_WR_DATA(0x1A);
	LCD_WR_DATA(0x33);
	LCD_WR_DATA(0x44);
	LCD_WR_DATA(0x44);
	LCD_WR_DATA(0x3F);
	LCD_WR_DATA(0x1C);
	LCD_WR_DATA(0x1B);
	LCD_WR_DATA(0x1D);
	LCD_WR_DATA(0x21);

	LCD_WR_REG(0x35);
	LCD_WR_DATA(0x00);

	LCD_SetDirection(4); // 设置液晶的扫描方向

	LCD_WR_REG(0x29);
}
// 设置液晶的扫描方向
// 1 竖屏
// 2 反向竖屏
// 3 横屏
// 4 反向横屏
void LCD_SetDirection(uint8_t option)
{
	switch (option)
	{
	case 1:
	{
		LCD_WIDTH = LCD_X;
		LCD_HEIGHT = LCD_Y;
		LCD_WR_REG(0x36);  // 设置旋转方向
		LCD_WR_DATA(0x00); // 正常竖屏
		break;
	}
	case 2:
	{
		LCD_WIDTH = LCD_X;
		LCD_HEIGHT = LCD_Y;
		LCD_WR_REG(0x36);  // 设置旋转方向
		LCD_WR_DATA(0xD4); // 反向竖屏
		break;
	}
	case 3:
	{
		LCD_WIDTH = LCD_Y;
		LCD_HEIGHT = LCD_X;
		LCD_WR_REG(0x36);  // 设置旋转方向
		LCD_WR_DATA(0xA0); // 正常横屏
		break;
	}
	case 4:
	{
		LCD_WIDTH = LCD_Y;
		LCD_HEIGHT = LCD_X;
		LCD_WR_REG(0x36);  // 设置旋转方向
		LCD_WR_DATA(0x60); // 反向横屏
		break;
	}
	default:
	{
		break;
	}
	}
}

//液晶初始化
void LCD_Init(void)
{
	// LCD_ILI9341_Init();
	LCD_ST7789V_Init();
	LCD_Clear(GREEN);
}

//LCD开启显示
void LCD_DisplayOn(void)
{
	LCD_WR_REG(0X29); //开启显示
}
//LCD关闭显示
void LCD_DisplayOff(void)
{
	LCD_WR_REG(0X28); //关闭显示
}

//设置坐标范围
//x1 y1  初始位置
//x2 y2  结束位置
void Address_set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	LCD_WR_REG(0X2A);
	LCD_WR_DATA(x1 >> 8);
	LCD_WR_DATA(x1 & 0XFF);
	LCD_WR_DATA(x2 >> 8);
	LCD_WR_DATA(x2 & 0XFF);

	LCD_WR_REG(0X2B);
	LCD_WR_DATA(y1 >> 8);
	LCD_WR_DATA(y1 & 0XFF);
	LCD_WR_DATA(y2 >> 8);
	LCD_WR_DATA(y2 & 0XFF);
	LCD_WR_REG(0X2C); //开始写入GRAM
}

//画点
//x,y:坐标
//POINT_COLOR:此点的颜色
void LCD_DrawPoint(uint16_t x, uint16_t y, uint16_t color)
{
	Address_set(x, y, x, y); //设置光标位置
	LCD_WR_DATA(color >> 8);
	LCD_WR_DATA(color & 0xFF);
}

//清屏函数
//color:要清屏的填充色
void LCD_Clear(uint16_t color)
{
	uint16_t i, j;
	Address_set(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
	for (i = 0; i < LCD_WIDTH; i++)
	{
		for (j = 0; j < LCD_HEIGHT; j++)
		{
			LCD_WR_DATA(color >> 8);
			LCD_WR_DATA(color & 0xFF);
		}
	}
}
//在指定区域内填充单个颜色
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)
//color:要填充的颜色
void LCD_Fill(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t color)
{
	uint16_t i, j;
	uint16_t xlen = 0;
	uint16_t ylen = 0;

	xlen = ex - sx + 1;
	ylen = ey - sy + 1;

	Address_set(sx, sy, ex, ey);
	for (i = 0; i < xlen; i++)
	{
		for (j = 0; j < ylen; j++)
		{
			LCD_WR_DATA(color >> 8);
			LCD_WR_DATA(color & 0xFF);
		}
	}
}

//画线
//x1,y1:起点坐标
//x2,y2:终点坐标
void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	uint16_t t;
	int xerr = 0, yerr = 0, delta_x, delta_y, distance;
	int incx, incy, uRow, uCol;
	delta_x = x2 - x1; //计算坐标增量
	delta_y = y2 - y1;
	uRow = x1;
	uCol = y1;
	if (delta_x > 0)
		incx = 1; //设置单步方向
	else if (delta_x == 0)
		incx = 0; //垂直线
	else
	{
		incx = -1;
		delta_x = -delta_x;
	}
	if (delta_y > 0)
		incy = 1;
	else if (delta_y == 0)
		incy = 0; //水平线
	else
	{
		incy = -1;
		delta_y = -delta_y;
	}
	if (delta_x > delta_y)
		distance = delta_x; //选取基本增量坐标轴
	else
		distance = delta_y;
	for (t = 0; t <= distance + 1; t++) //画线输出
	{
		LCD_DrawPoint(uRow, uCol, POINT_COLOR); //画点
		xerr += delta_x;
		yerr += delta_y;
		if (xerr > distance)
		{
			xerr -= distance;
			uRow += incx;
		}
		if (yerr > distance)
		{
			yerr -= distance;
			uCol += incy;
		}
	}
}
//画矩形
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	LCD_DrawLine(x1, y1, x2, y1);
	LCD_DrawLine(x1, y1, x1, y2);
	LCD_DrawLine(x1, y2, x2, y2);
	LCD_DrawLine(x2, y1, x2, y2);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void LCD_Draw_Circle(uint16_t x0, uint16_t y0, uint8_t r)
{
	int a, b;
	int di;
	a = 0;
	b = r;
	di = 3 - (r << 1); //判断下个点位置的标志
	while (a <= b)
	{
		LCD_DrawPoint(x0 + a, y0 - b, POINT_COLOR); //5
		LCD_DrawPoint(x0 + b, y0 - a, POINT_COLOR); //0
		LCD_DrawPoint(x0 + b, y0 + a, POINT_COLOR); //4
		LCD_DrawPoint(x0 + a, y0 + b, POINT_COLOR); //6
		LCD_DrawPoint(x0 - a, y0 + b, POINT_COLOR); //1
		LCD_DrawPoint(x0 - b, y0 + a, POINT_COLOR);
		LCD_DrawPoint(x0 - a, y0 - b, POINT_COLOR); //2
		LCD_DrawPoint(x0 - b, y0 - a, POINT_COLOR); //7
		a++;
		//使用Bresenham算法画圆
		if (di < 0)
			di += 4 * a + 6;
		else
		{
			di += 10 + 4 * (a - b);
			b--;
		}
	}
}
//在指定位置显示一个字符
//x,y:起始坐标
//num:要显示的字符:" "--->"~"
//size:字体大小 12/16/24
//mode:叠加方式(1)还是非叠加方式(0)
void LCD_ShowChar(uint16_t x, uint16_t y, uint8_t num, uint8_t size, uint8_t mode)
{
	uint8_t temp, t1, t;
	uint16_t y0 = y;
	uint8_t csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size / 2); //得到字体一个字符对应点阵集所占的字节数
	num = num - ' ';												//得到偏移后的值（ASCII字库是从空格开始取模，所以-' '就是对应字符的字库）

	//	Address_set(x,y,x+size-1,y+size-1);
	for (t = 0; t < csize; t++)
	{
		if (size == 12)
			temp = asc2_1206[num][t]; //调用1206字体
		else if (size == 16)
			temp = asc2_1608[num][t]; //调用1608字体
		else if (size == 24)
			temp = asc2_2412[num][t]; //调用2412字体
		else
			return; //没有的字库
		for (t1 = 0; t1 < 8; t1++)
		{
			if (temp & 0x80)
				LCD_DrawPoint(x, y, POINT_COLOR);
			else if (mode == 0)
				LCD_DrawPoint(x, y, BACK_COLOR);
			temp <<= 1;
			y++;
			if ((y - y0) == size)
			{
				y = y0;
				x++;
				break;
			}
		}
	}
}
//m^n函数
//返回值:m^n次方.
uint32_t LCD_Pow(uint8_t m, uint8_t n)
{
	uint32_t result = 1;
	while (n--)
		result *= m;
	return result;
}
//显示数字,高位为0,则不显示
//x,y :起点坐标
//len :数字的位数
//size:字体大小
//color:颜色
//num:数值(0~4294967295);
void LCD_ShowNum(uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint8_t size)
{
	uint8_t t, temp;
	uint8_t enshow = 0;
	for (t = 0; t < len; t++)
	{
		temp = (num / LCD_Pow(10, len - t - 1)) % 10;
		if (enshow == 0 && t < (len - 1))
		{
			if (temp == 0)
			{
				LCD_ShowChar(x + (size / 2) * t, y, ' ', size, 0);
				continue;
			}
			else
				enshow = 1;
		}
		LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size, 0);
	}
}
//显示数字,高位为0,还是显示
//x,y:起点坐标
//num:数值(0~999999999);
//len:长度(即要显示的位数)
//size:字体大小
//mode:
//[7]:0,不填充;1,填充0.
//[6:1]:保留
//[0]:0,非叠加显示;1,叠加显示.
void LCD_ShowxNum(uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint8_t size, uint8_t mode)
{
	uint8_t t, temp;
	uint8_t enshow = 0;
	for (t = 0; t < len; t++)
	{
		temp = (num / LCD_Pow(10, len - t - 1)) % 10;
		if (enshow == 0 && t < (len - 1))
		{
			if (temp == 0)
			{
				if (mode & 0X80)
					LCD_ShowChar(x + (size / 2) * t, y, '0', size, mode & 0X01);
				else
					LCD_ShowChar(x + (size / 2) * t, y, ' ', size, mode & 0X01);
				continue;
			}
			else
				enshow = 1;
		}
		LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size, mode & 0X01);
	}
}
//显示字符串
//x,y:起点坐标
//size:字体大小
//*str:字符串起始地址
void LCD_ShowString(uint16_t x, uint16_t y, uint8_t size, uint8_t *str)
{
	while ((*str <= '~') && (*str >= ' ')) //判断是不是非法字符!
	{
		LCD_ShowChar(x, y, *str, size, 0);
		x += size / 2;
		str++;
	}
}

void LCD_ShowChinese(uint16_t x, uint16_t y, uint16_t index)
{
	uint8_t i, j, k, temp;
	for (i = 0; i < 16; i++)
	{
		for (j = 0; j < 2; j++)
		{
			temp = chinese_16x16[index][i * 2 + j];
			for (k = 0; k < 8; k++)
			{
				if (temp & 0x80)
					LCD_DrawPoint(x + 8 * j + k, y + i, POINT_COLOR);
				else
					LCD_DrawPoint(x + 8 * j + k, y + i, BACK_COLOR);
				temp <<= 1;
			}
		}
	}
}
