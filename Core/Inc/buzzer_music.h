/**
  ******************************************************************************
  * @file    buzzer_music.h
  * @brief   This file contains all the function prototypes for
  *          the buzzer_music.c file
  ******************************************************************************
  * @attention
  *
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BUZZER_MUSIC_H__
#define __BUZZER_MUSIC_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

void play_music(void);

#ifdef __cplusplus
}
#endif

#endif /* __BUZZER_MUSIC_H__ */

/****END OF FILE****/
