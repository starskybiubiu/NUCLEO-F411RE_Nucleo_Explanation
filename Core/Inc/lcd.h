#ifndef __LCD_H
#define __LCD_H
#include "main.h"

// 颜色定义
#define WHITE 0xFFFF
#define BLACK 0x0000
#define BLUE 0x001F
#define BRED 0XF81F
#define GRED 0XFFE0
#define GBLUE 0X07FF
#define RED 0xF800
#define MAGENTA 0xF81F
#define GREEN 0x07E0
#define CYAN 0x7FFF
#define YELLOW 0xFFE0
#define BROWN 0XBC40 //棕色
#define BRRED 0XFC07 //棕红色
#define GRAY 0X8430	 //灰色
// 尺寸定义
#define LCD_X 240
#define LCD_Y 320
extern uint16_t LCD_WIDTH;
extern uint16_t LCD_HEIGHT;

// LCD的画笔颜色和背景色
extern uint16_t POINT_COLOR; //默认红色
extern uint16_t BACK_COLOR;	 //背景颜色.默认为白色

// ILI9341控制器屏幕初始化
void LCD_ILI9341_Init(void);
// ST7789V控制器屏幕初始化
void LCD_ST7789V_Init(void);
//液晶初始化
void LCD_Init(void);
//写寄存器函数
//reg:寄存器值
void LCD_WR_REG(uint8_t reg);
//写LCD数据
//data:要写入的值
void LCD_WR_DATA(uint8_t data);
// 设置液晶的扫描方向
void LCD_SetDirection(uint8_t option);
//LCD开启显示
void LCD_DisplayOn(void);
//LCD关闭显示
void LCD_DisplayOff(void);
//设置坐标范围
//x1 y1  初始位置
//x2 y2  结束位置
void Address_set(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
//画点
//x,y:坐标
//POINT_COLOR:此点的颜色
void LCD_DrawPoint(uint16_t x, uint16_t y, uint16_t color);
//清屏函数
//color:要清屏的填充色
void LCD_Clear(uint16_t color);
//在指定区域内填充单个颜色
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)
//color:要填充的颜色
void LCD_Fill(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t color);
//画线
//x1,y1:起点坐标
//x2,y2:终点坐标
void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
//画矩形
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void LCD_Draw_Circle(uint16_t x0, uint16_t y0, uint8_t r);
//在指定位置显示一个字符
//x,y:起始坐标
//num:要显示的字符:" "--->"~"
//size:字体大小 12/16/24
//mode:叠加方式(1)还是非叠加方式(0)
void LCD_ShowChar(uint16_t x, uint16_t y, uint8_t num, uint8_t size, uint8_t mode);
//显示数字,高位为0,则不显示
//x,y :起点坐标
//len :数字的位数
//size:字体大小
//color:颜色
//num:数值(0~4294967295);
void LCD_ShowNum(uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint8_t size);
//显示数字,高位为0,还是显示
//x,y:起点坐标
//num:数值(0~999999999);
//len:长度(即要显示的位数)
//size:字体大小
//mode:
//[7]:0,不填充;1,填充0.
//[6:1]:保留
//[0]:0,非叠加显示;1,叠加显示.
void LCD_ShowxNum(uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint8_t size, uint8_t mode);
//显示字符串
//x,y:起点坐标
//size:字体大小
//*str:字符串起始地址
void LCD_ShowString(uint16_t x, uint16_t y, uint8_t size, uint8_t *str);
// 显示一个16x16大小的中文字符,其中index为中文字符所在字库的下标
void LCD_ShowChinese(uint16_t x, uint16_t y,uint16_t index);

#endif
