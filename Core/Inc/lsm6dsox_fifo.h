#ifndef LSM6DSOX_FIFO_H
#define LSM6DSOX_FIFO_H

#include "main.h"
#include "lsm6dsox_reg.h"

extern stmdev_ctx_t dev_ctx;
extern float acceleration_mg[3];
extern float angular_rate_mdps[3];
extern uint8_t tx_buffer[1000];

void lsm6dsox_fifo_init(void);
void lsm6dsox_fifo_run(void);

#endif
